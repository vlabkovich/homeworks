﻿using MvcApplication7.Ado;
using MvcApplication7.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcApplication7.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index(int? id)
        {
            if (id == null)
                id = 1;
            GoodsDal gd = new GoodsDal();
            List<Catalogs> catalogs = gd.GetCatalogs();
            List<Goods> goods = gd.GetGoods(id);
            var model = new Catgoods { Catalogs = catalogs, Goods = goods };
            return View(model);
        }

        public ActionResult Contacts()
        {
            GoodsDal gd = new GoodsDal();
            List<Catalogs> catalogs = gd.GetCatalogs();
            return View(catalogs);
        }

        public ActionResult About()
        {
            return View();
        }
    }
}
