﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcApplication7.Models
{
    public class Catgoods
    {
        public IEnumerable<Catalogs> Catalogs { get; set; }
        public IEnumerable<Goods> Goods { get; set; }
    }
}