﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcApplication7.Models
{
    public class Catalogs
    {
        public int CatalogId { get; set; }
        public string Name { get; set; }
    }
}