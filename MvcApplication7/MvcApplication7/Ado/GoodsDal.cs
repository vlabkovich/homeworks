﻿using MvcApplication7.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MvcApplication7.Ado
{
    public class GoodsDal
    {
        public string ConnString
        {
            get
            {
                return ConfigurationManager.AppSettings["connString"];
            }
        }

        public List<Catalogs> GetCatalogs()
        {
            List<Catalogs> catalogs = new List<Catalogs>();
            string query = "select * from Catalogs";

            SqlConnection cnn = new SqlConnection(ConnString);
            SqlCommand cmd = new SqlCommand(query, cnn);
            cnn.Open();
            SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            
            object[] data;
            while (reader.Read())
            {
                data = new object[reader.FieldCount];
                reader.GetValues(data);
                catalogs.Add(new Catalogs()
                {
                    CatalogId = (int)data[0],
                    Name = data[1].ToString()
                });
            }
            reader.Close();
            return catalogs;
        }

        public List<Goods> GetGoods(int? id)
        {
            
            List<Goods> goods = new List<Goods>();
            string query = String.Format("select * from Goods where CatalogId = {0}", id);

            SqlConnection cnn = new SqlConnection(ConnString);
            SqlCommand cmd = new SqlCommand(query, cnn);
            cnn.Open();
            SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);

            object[] data;
            while (reader.Read())
            {
                data = new object[reader.FieldCount];
                reader.GetValues(data);
                goods.Add(new Goods()
                {
                    Id = (int)data[0],
                    CatalogId = (int)data[1],
                    Brand = data[2].ToString(),
                    Model = data[3].ToString(),
                    Price = (int)data[4]
                });
            }
            reader.Close();
            return goods;
        }
    }
}