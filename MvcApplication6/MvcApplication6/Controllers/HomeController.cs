﻿using MvcApplication6.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcApplication6.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index(int? id)
        {
            if (id == null)
                id = 1;
            GDBDataContext catalog = new GDBDataContext();
            var name = from n in catalog.Catalogs
                       select n;
            var goods = from g in catalog.Goods
                        where g.CatalogId == id
                        select g;

            var model = new Catgoods {Catalog = name, Good = goods};
            return View(model);
        }

        public ActionResult Contacts()
        {
            GDBDataContext catalog = new GDBDataContext();
            var name = from n in catalog.Catalogs
                       select n;


            return View(name);
        }

        public ActionResult About()
        {
            return View();
        }

    }
}
