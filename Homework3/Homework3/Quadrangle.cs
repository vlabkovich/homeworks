﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework3
{
    abstract class Quadrangle
    {
        public double a, b, c, d;
        public abstract double Square();
        public abstract double Perimeter();
        public abstract double Diagonal1();
        public abstract double Diagonal2();

    }

    class Rectangle : Quadrangle
    {
        public Rectangle()
        {
            a = c = 5;
            b = d = 10;
        }
        public override double Square()
        {
            return a * b;
        }

        public override double Perimeter()
        {
            return 2 * (a + b);
        }

        public override double Diagonal1()
        {
            return Math.Sqrt(Math.Pow(a,2) + Math.Pow(b,2));
        }

        public override double Diagonal2()
        {
            return Math.Sqrt(Math.Pow(a, 2) + Math.Pow(b, 2));
        }

    }

    class Rhomb : Quadrangle
    {
        double S, D1, D2, angle;
        const double pi = 3.1415926535897932384626433832795;
        public Rhomb()
        {
            a = b = c = d = 5;
            angle = 60; //угол между сторонами

        }

        public override double Square()
        {
            S = Math.Pow(a,2)*Math.Sin(angle*pi/180);
            return S;

        }

        public override double Perimeter()
        {
            return 4 * a;
        }

        public override double Diagonal1()
        {
            D1 = Math.Sqrt(2 * S / Math.Tan(angle*pi/360));
            return D1;
        }
        public override double Diagonal2()
        {
            D2 = 2 * S / D1;
            return D2;
        }
    }

    class Trapezium : Quadrangle
    {
        double h;
        const double pi = 3.1415926535897932384626433832795;
        public Trapezium()
        {
            a = 15;
            b = 10;
            c = 5;
            d = 7;
            h = 4;
        }

        public override double Square()
        {
            return (a+b)*h/2;
        }
        
        public override double Perimeter()
        {
            return a + b + c + d;
        }

        public override double Diagonal1()
        {
            return Math.Sqrt(Math.Pow(a,2)+Math.Pow(d,2)-2*a*d*Math.Sqrt(1-Math.Pow(h/d,2)));
        }

        public override double Diagonal2()
        {
            return Math.Sqrt(Math.Pow(a,2)+Math.Pow(c,2)-2*a*c*Math.Sqrt(1-Math.Pow(h/c,2)));
        }
    }
}
