﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework3
{
    class Program
    {
        static void Main(string[] args)
        {
            Rectangle r = new Rectangle();
            Console.WriteLine("Площадь прямоугольника = {0}",r.Square());
            Console.WriteLine("Периметр прямоугольника = {0}",r.Perimeter());
            Console.WriteLine("Первая диагональ = {0}",r.Diagonal1());
            Console.WriteLine("Вторая диагональ = {0}",r.Diagonal2());
            Console.WriteLine();

            Rhomb ro = new Rhomb();
            Console.WriteLine("Площадь ромба = {0}", ro.Square());
            Console.WriteLine("Периметр ромба = {0}", ro.Perimeter());
            Console.WriteLine("Первая диагональ = {0}", ro.Diagonal1());
            Console.WriteLine("Вторая диагональ = {0}", ro.Diagonal2());
            Console.WriteLine();

            Trapezium t = new Trapezium();
            Console.WriteLine("Площадь трапеции = {0}", t.Square());
            Console.WriteLine("Периметр трапеции = {0}", t.Perimeter());
            Console.WriteLine("Первая диагональ = {0}", t.Diagonal1());
            Console.WriteLine("Вторая диагональ = {0}", t.Diagonal2());
            Console.WriteLine();


        }
    }
}
