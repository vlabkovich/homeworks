﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework4
{
    delegate void MusicAlbumDelegate();
    class MusicAlbum : IDisposable
    {
        int _size;
        string[] A;
        public event MusicAlbumDelegate PlayEvent;
        public event MusicAlbumDelegate StopEvent;

        public MusicAlbum(int size)
        {
            _size = size;
            A = new String[size];
        }

        public void Push()
        {
            int j = 1;
            for (int i = 0; i < _size; i++)
            {
                Console.Write("Введите {0} песню: ", j);
                A[i] = Console.ReadLine();
                j++;
                

            }
        }

        public void Pop()
        {
            PlayEvent();
            Console.WriteLine("Список песен:");
            int j = _size;
            for (int i = (_size-1); i >= 0; i--)
            {
                Console.WriteLine("{0}. {1}", j, A[i]);
                j--;
            }
            StopEvent();
        }

        public void Dispose()
        {
            A = null;
            Console.WriteLine("Dispose is cleaning resourses");
        }
    }
}
