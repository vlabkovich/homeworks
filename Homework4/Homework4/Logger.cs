﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework4
{
    class Logger
    {
        public void PlayStart()
        {
            Console.WriteLine("Play started");
        }

        public void PlayStop()
        {
            Console.WriteLine("Play stopped");
        }
    }
}
