﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Количество песен в альбоме: ");
            int size = Convert.ToInt32(Console.ReadLine());
            Logger l = new Logger();
            using (MusicAlbum a = new MusicAlbum(size))
            {
                a.PlayEvent += l.PlayStart;
                a.StopEvent += l.PlayStop;
                a.Push();
                a.Pop();                
            }
            
        }
    }
}
