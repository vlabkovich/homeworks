﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Circle
{
    class Figure
    {
        double r; //по умолчанию private
        double p;

        public double pi
        {
            get { return p; }
        }
        
        public double R
        {
            get {return r;}
            set
            {
                if (value > 0)
                r = value;
            }
        }

        public Figure()
        {
            p = 3.14;
        }

        public Figure(double _r):this()
        {
            r = _r;
        }

        public double Square()
        {
            return p * r * r;
        }

        public double Perimeter()
        {
            return 2 * p * r;
        }
        
    }
}
