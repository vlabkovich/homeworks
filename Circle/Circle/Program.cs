﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Circle
{
    class Program
    {
        static void Main(string[] args)
        {
            Figure f = new Figure(100);
            f.R = -1;
            
            Console.WriteLine(f.Square());
            Console.WriteLine(f.Perimeter());
        }
    }
}
