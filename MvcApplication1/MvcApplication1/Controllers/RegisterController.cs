﻿using MvcApplication1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcApplication1.Controllers
{
    public class RegisterController : Controller
    {
        //
        // GET: /Register/

        public ActionResult Register()
        {
            Man m = new Man();
            return View(m);
        }

        [HttpPost]
        public ActionResult Register(Man m)
        {
            if (!ModelState.IsValid)
                return View();
            return View(m);
        }            

    }
}
