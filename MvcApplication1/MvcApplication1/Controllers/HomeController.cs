﻿using MvcApplication1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcApplication1.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index(string id)
        {
            Man man = new Man();
            man.Name = id;
            man.Surname = "Surname" + id;

            return View(man);
        }

        [HttpPost]
        public ActionResult Index(Man man)
        {
            if (!ModelState.IsValid)
                return View();
            return View(man);
        }
        
        //public string Index()
        //{
        //    return "Hello from Vasily!";
        //}

        //public string Register(string man)
        //{
        //    return "Hello from " + man;
        //}

        public string Register(string id) //можно передавать параметры через /
        {
            return "Hello from " + id;
        }
    }
}
