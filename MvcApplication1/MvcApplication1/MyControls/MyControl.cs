﻿using System.Web.Mvc;

    public static class MyControl
    {
        public static MvcHtmlString MyRadioButton(this HtmlHelper helper)
        {
            return new MvcHtmlString("<input type='radio'>");
        }

        public static MvcHtmlString CountrySelect(this HtmlHelper helper)
        {
            return new MvcHtmlString(@"<select>
        <option>Belarus</option>
        <option>Ukraine</option>
        <option>Russia</option>
    </select>");
        }
    }
