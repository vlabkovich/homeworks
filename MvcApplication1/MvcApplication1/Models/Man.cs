﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcApplication1.Models
{
    public class Man
    {
        [Required(ErrorMessage = "Name is required")]
        [StringLengthAttribute(15, ErrorMessage = "Length should be less than 15")]
        public string Name { get; set; }
        
        [StringLengthAttribute(10, ErrorMessage = "Length should be less than 10")]
        public string Surname { get; set; }

        [Range(20, 150, ErrorMessage = "Age must be between 20 and 150")]
        public int Age { get; set; }
    }
}