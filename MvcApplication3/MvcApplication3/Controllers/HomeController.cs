﻿using MvcApplication3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcApplication3.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(Userinfo ui)
        {
            Session["Name"] = ui.UserName;
            return View(ui);
        }

        public string Contacts()
        {
            return "Здравствуйте, " + Session["Name"];
        }

        public string About()
        {
            return "Здравствуйте, " + Session["Name"];
        }

    }
}
