﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlbumNamespace
{
    [Serializable()]
    public class AlbumFullException : SystemException
    {
        public AlbumFullException() : base() { }
        public AlbumFullException(string message) : base(message) { }
        public AlbumFullException(string message, System.Exception inner) : base(message, inner) { }

        protected AlbumFullException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContextStates context) { }
    }
}
