﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AlbumNamespace
{
    public static class Logger
    {
        
        public static void Log(string message)
        {
            bool CreatedNew;
            Mutex mutex = new Mutex(true, "MyMutex", out CreatedNew);
            if (CreatedNew == false)
                throw new Exception("Такой мьютекс уже существует !!!");
            using (StreamWriter writer = new StreamWriter("Log.txt", true))
            {
                writer.WriteLine(DateTime.Now.ToString() + " " + message);
                Console.WriteLine("Logger: " + message);
            }
            mutex.ReleaseMutex();
            
            
        }

        public static void LogError(string message)
        {
            bool CreatedNew;
            Mutex mutex = new Mutex(true, "MyMutex", out CreatedNew);
            if (CreatedNew == false)
                throw new Exception("Такой мьютекс уже существует !!!");
            mutex.WaitOne();
            using (StreamWriter writer = new StreamWriter("Log.txt", true))
            {
                writer.WriteLine(DateTime.Now.ToString() + " Error: " + message);
                Console.WriteLine("Error: " + message);
            }
            mutex.ReleaseMutex();

        }
    }
}
