﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;



namespace AlbumNamespace
{
    public delegate void LoggerDelegate(string message);
    [DataContract]
    public class Album
    {
        [DataMember]
        public string AlbumName { get; set; }
        [DataMember]
        List<Track> tracks = new List<Track>();
        int capacity = 4;
        public event LoggerDelegate playEvent;

        public void AddTrack(Track track) // push
        {
            try
            {
                if (tracks.Count > capacity)
                    throw new AlbumFullException();
                tracks.Add(track);
            }
            catch (AlbumFullException)
            {
                Console.WriteLine("AlbumFullException is occured");
            }
        }
        public Track GetTrack() // pop
        {
            int lastTrack = tracks.Count - 1;
            Track trackToReturn = tracks[lastTrack];
            tracks.Remove(trackToReturn);
            return trackToReturn;
        }

        public void Play()
        {
            if (playEvent != null)
            {
                playEvent("Start playing");
            }
            while (!AlbumIsEmpty())
            {
                Track track = GetTrack();
                track.Play();
            
            }
            playEvent("Stop playing");
        }

        public bool AlbumIsEmpty()
        {
            return tracks.Count == 0;
        }
    }
}
