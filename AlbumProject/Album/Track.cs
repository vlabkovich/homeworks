﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using NAudio.Wave;
using System.Threading;

namespace AlbumNamespace
{
    [DataContract]
    public class Track
    {
        string _name;
        string _author;
        Genre _genre;
        DateTime _release;
                
        public Track(string name, string author, Genre genre, DateTime release)
        {
            _name = name;
            _author = author;
            _genre = genre;
            _release = release;
        }

        public void Play()
        {
            Console.WriteLine("Track "+Name+" is playing.");

            var rdr = new Mp3FileReader(Name);
            var wavStream = WaveFormatConversionStream.CreatePcmStream(rdr);
            var waveout = new WaveOut(WaveCallbackInfo.FunctionCallback());
            waveout.Init(wavStream);
            waveout.Play();
            while (wavStream.Position != wavStream.Length)
                Thread.Sleep(1000);
            Console.WriteLine("Track " +Name+ " ended.");
        }

        [DataMember]
        public string Name
        {
            get { return _name; }
            set
            {
                if (!String.IsNullOrEmpty(value) && value.Length > 256)
                {
                    _name = value.Substring(0, 256);
                }
                else
                {
                    _name = value;
                }
            }
        }

        [DataMember]
        public string Author
        {
            get { return _author; }
            set { _author = value; }
        }

        [DataMember]
        public Genre Genre
        {
            get { return _genre; }
            set { _genre = value; }
        }
        
        [DataMember]
        public DateTime Release
        {
            get { return _release; }
            set { _release = value; }
        }
    }
}
