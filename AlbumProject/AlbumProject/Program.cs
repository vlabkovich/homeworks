﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlbumNamespace;
using System.Runtime.Serialization;
using System.IO;

namespace AlbumProject
{
    class Program
    {
        static void Main(string[] args)
        {
            Album album = new Album();
            album.AlbumName = "Sborka";
            try
            {
                album.AddTrack(new Track("1.mp3", "A1", Genre.Classic, DateTime.Now));
                album.AddTrack(new Track("2.mp3", "A2", Genre.Pop, DateTime.Now));
                album.AddTrack(new Track("3.mp3", "A3", Genre.Rock, DateTime.Now));
                album.AddTrack(new Track("4.mp3", "A4", Genre.Classic, DateTime.Now));
                album.AddTrack(new Track("5.mp3", "A5", Genre.Classic, DateTime.Now));
                //album.AddTrack(new Track("N6", "A6", Genre.Classic, DateTime.Now));
                
            }

            catch (OutOfMemoryException)
            {
                Console.WriteLine("Out of Memory exception is occured!");
            }
            catch (Exception)
            {
                Console.WriteLine("Exception clr is occured!");
            }
            catch
            {
                Console.WriteLine("All exception is occured!");
            }

            var ds = new NetDataContractSerializer();
            using (Stream s = File.Create("Album.xml"))
            {
                ds.WriteObject(s, album);
            }
                        
            album.playEvent += Logger.Log;
            album.Play();

            Console.ReadLine();
            
        }
    }
}
